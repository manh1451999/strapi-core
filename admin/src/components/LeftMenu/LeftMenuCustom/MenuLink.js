import React, { useMemo } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link, withRouter } from 'react-router-dom';
import { startsWith } from 'lodash';

function MenuLink({ destination, icon, isDisplayed, label, search }) {

  const isLinkActive = search?.includes('dashBoardType') ? location?.href?.includes(search) : startsWith(
    location.pathname.replace('/admin', '').concat('/'),
    destination.concat('/')
  )
  //  useMemo(() => {
  //   console.log('location?.href', location?.href)
  //   if (location?.href?.includes('dashBoardType')) {
  //     return location?.href?.includes(search)
  //   }
  //   return startsWith(
  //     location.pathname.replace('/admin', '').concat('/'),
  //     destination.concat('/')
  //   )
  // }, [location.href])
  return (
    <Link className={`menu-link-section_item ${isLinkActive ? 'active' : ''}`} as={Link}
      to={{
        pathname: destination,
        search,
      }}>
      <div className="menu-link-section_item-icon"> <FontAwesomeIcon icon={icon} style={{ marginRight: '12px' }} /></div>
      <div className="menu-link-section_item-link">{label}</div>
    </Link>
  )
}

export default withRouter(MenuLink)
