import React from 'react'
import MenuLink from './MenuLink'

export default function MenuLinkSection({title, collectionLinks}) {
  return (
    <div className="menu-link-section">
      <div className="menu-link-section_title">
        {title}
      </div>
      <div className="menu-link-section_wrap-item">
        {collectionLinks.map((collectionLink, index) => {
          return <MenuLink key={`MenuLink.${index}`} {...collectionLink}/>
        })}
      </div>
    </div>
  )
}
