import React, { useMemo } from 'react'
import styled from 'styled-components';
import { LeftMenuHeader } from '../../components/LeftMenu';
import MenuLinkSection from '../../components/LeftMenu/LeftMenuCustom/MenuLinkSection';
import { ROLE } from '../../utils/constants';
import getRoleUser from '../../utils/getRoleUser';
import './customMenu.css'
export default function LeftMenuCustomContainer({filteredCollectionTypeLinks, filteredSingleTypeLinks}) {

  // liệt kê menu dưới dạng các group, các thành phần không có thì mặc định hiển thị trong các thành phần khác, đặt isDisplayed là false nếu muốn ẩn khỏi menu
  const menuDefault = [
    {
      title: "Quản lý trang",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/singleType/application::home-page.home-page",
          "isDisplayed": true,
          "label": "Trang homepage",
          "search": ""
        },

      ]
    },
    {
      title: "Title list menu",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::test.test",
          "isDisplayed": true,
          "label": "Test",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },

      ]
    },
    {
      title: "Quản lý địa chỉ",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::city.city",
          "isDisplayed": false,  // ẩn
          "label": "Thành phổ",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::district.district",
          "isDisplayed": false,  // ẩn
          "label": "Danh sách quận huyện",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
      ]
    },

    {
      title: "Quản lý trạng thái",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::status.status",
          "isDisplayed": false,
          "label": "Trạng thái",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },

      ]
    },
    {
      title: "Người dùng",
      collectionLinks: [

        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/plugins::users-permissions.user",
          "isDisplayed": true,
          "label": "Người dùng ứng dụng",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/settings/users",
          "isDisplayed": true,
          "label": "Người dùng hệ thống",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
      ]
    },
    {
      title: "Các trang ẩn",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::status.status",
          "isDisplayed": false,
          "label": "Trạng thái",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },

      ]
    },

  ]

  const otherMenu = useMemo(()=>{
    let results = []
    let listItemMenuCustom = [].concat(...menuDefault.map(item=> item.collectionLinks))
    let listItemMenuCollectionOther = filteredCollectionTypeLinks.filter(menu=> !listItemMenuCustom.find(x=>menu.destination == x.destination))
    let listItemMenuSingleOther = filteredSingleTypeLinks.filter(menu=> !listItemMenuCustom.find(x=>menu.destination == x.destination))
    if(listItemMenuCollectionOther.length > 0) results.push({title: "Các thành phần khác", collectionLinks: listItemMenuCollectionOther})
    if(listItemMenuSingleOther.length > 0) results.push({title: "Các trang khác", collectionLinks: listItemMenuSingleOther})
    return results
  }, [filteredCollectionTypeLinks, filteredSingleTypeLinks])

  const menuEditor = [{
    title: "Plugins",
    collectionLinks: [
      {
        destination: "/plugins/upload",
        icon: "cloud-upload-alt",
        isDisplayed: true,
        label: "Media Libarary",
      }
    ]
  }
 ]
  const menuAdmin = [{
    title: "Plugins",
    collectionLinks: [
      {
        destination: "/plugins/content-type-builder",
        icon: "paint-brush",
        isDisplayed: true,
        label: "Content-Types Builder",
      },
      {
        destination: "/plugins/documentation",
        icon: "book",
        isDisplayed: true,
        label: "Documentation",
      },
      {
        destination: "/plugins/upload",
        icon: "cloud-upload-alt",
        isDisplayed: true,
        label: "Media Libarary",
      }
    ]
  },
  {
    title: "General",
    collectionLinks: [
      {
        destination: "/list-plugins",
        icon: "list",
        isDisplayed: true,
        label: "Plugins",
      },
      {
        destination: "/marketplace",
        icon: "shopping-basket",
        isDisplayed: true,
        label: "Marketplace",
      },
      {
        destination: "/settings",
        icon: "cog",
        isDisplayed: true,
        label: "Settings",
      }
    ]
  }]

  // const menu = useMemo(() => {
  //   let role = getRoleUser();
  //   if (role === ROLE.SUPER_ADMIN) return [...menuDefault, ...menuAdmin]
  //   else if (role === ROLE.EDITOR) return [...menuDefault, ...menuEditor]
  //   return menuDefault
  // }, [])

  const menu = useMemo(() => {
    let role = getRoleUser();
    if (role === ROLE.SUPER_ADMIN) return [...menuDefault, ...otherMenu, ...menuAdmin]
    else if (role === ROLE.EDITOR) return [...menuDefault, ...otherMenu, ...menuEditor]
    return menuDefault
  }, [otherMenu])

  const menuFilter = useMemo(() => {
    return menu.map(item=>{
      let newItem = {...item}
      newItem.collectionLinks = newItem.collectionLinks.filter(collectionLink=>collectionLink?.isDisplayed)
      return newItem
    }).filter(item=> item?.collectionLinks?.length>0)
  }, [menu])



  return (
    <div className="left-menu-cunstom">
      <Wrapper>
        <LeftMenuHeader />
        <LinksContainer>
          {
            menuFilter.map((itemMenu, index) => {
              return <MenuLinkSection key={`menuLinkSection.${index}`} title={itemMenu?.title} collectionLinks={itemMenu?.collectionLinks} />
            })
          }


        </LinksContainer>
      </Wrapper>
    </div>

  );
};



const Wrapper = styled.div`
  position: fixed;
  float: left;
  top: 0;
  left: 0;
  height: 100vh;
  width: ${props => props.theme.main.sizes.leftMenu.width};
  background: #fff;

  /* scrollbar overrides */
  * {
    ::-webkit-scrollbar {
      width: 7px;
    }

    ::-webkit-scrollbar-track,
    ::-webkit-scrollbar-track:hover {
      background-color: transparent;
    }

    ::-webkit-scrollbar-thumb {
      background-color: ${props => props.theme.main.colors.leftMenu['title-color']};
    }

    ::-webkit-scrollbar-thumb:hover {
      background-color: ${props => props.theme.main.colors.leftMenu['link-color']};
    }

    /* firefox */
    scrollbar-color: ${props => props.theme.main.colors.leftMenu['title-color']} transparent;
  }
`;

const LinksContainer = styled.div`
  padding-top: 3rem;
  position: absolute;
  top: ${props => props.theme.main.sizes.leftMenu.height};
  right: 0;
  bottom: 0;
  left: 0;
  overflow-y: auto;
  height: calc(100vh - (${props => props.theme.main.sizes.leftMenu.height} + 3rem));
  box-sizing: border-box;
`;
