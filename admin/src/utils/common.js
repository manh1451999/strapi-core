export default {
  getItem: (key) => localStorage?.getItem(key) || sessionStorage?.getItem(key)
}
