import { ROLE } from "./constants";
import common from "../utils/common"

const getRoleUser = () => {
  try{
    let userInfo = JSON.parse(common.getItem('userInfo') || {})?.roles?.map(role=>role.code)
    if(!userInfo) return ROLE.GUEST;
    if(userInfo.includes(ROLE.SUPER_ADMIN)) return ROLE.SUPER_ADMIN
    if(userInfo.includes(ROLE.EDITOR)) return ROLE.EDITOR
    if(userInfo.includes(ROLE.AUTHOR)) return ROLE.AUTHOR
  } catch(err){
    return ROLE.GUEST;
  }
}

export default getRoleUser

