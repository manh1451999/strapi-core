'use strict';
const { sanitizeEntity } = require('strapi-utils');
const moment = require('moment')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
async function getMailTemplate(name) {
  let mailTemplates = await strapi
    .store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions',
      key: 'email',
    })
    .get();
  return mailTemplates[name];
}

async function sendEmailMemberRegistration(user) {
  const template = await getMailTemplate("member_registration");
  const templateSend = {
    subject: template.options.object,
    text: template.options.message,
    html: template.options.message,
  }
  await strapi.plugins.email.services.email.sendTemplatedEmail(
    {
      to: user.email || 'manh1451999@gmail.com'
    },
    templateSend,
    {
      ...user
    }
  )
}

module.exports = {
  async sendEmailMemberRegistration(ctx) {
    const params = ctx.request.body || {};
    // ['email', 'message', 'phone'].forEach(key => {
    //   if (!params[key]) {
    //     return ctx.badRequest(`Không thể bỏ trống ${key}.`);
    //   }
    // });
    // if (!emailRegExp.test(params.email)) return ctx.badRequest(`email không đúng định dạng`);


    const { name, email, phone, message } = params;

    // let checkSpam = await strapi.services["member-registration"].findOne({ email, created_at_gte: moment().subtract(1, "day").toISOString() });

    // if (checkSpam) return ctx.badRequest(`Không thể gửi quá nhiều đăng ký trong 24h`);

    const user = { name, email, phone, message }
    await sendEmailMemberRegistration(user).catch(err => {
      console.log('err', err)
      ctx.badRequest(`Không thể gửi email xác nhận đăng ký`)
    });
    return "THÀNH CÔNG"

    // let entity = await strapi.services["member-registration"].create(user);
    // return sanitizeEntity(entity, { model: strapi.models["member-registration"] });


  },
  async checkAuthFid(ctx) {
    if(ctx.state.user){
      return ctx.state.user
    }else ctx.badRequest(`Đã xảy ra lỗi trong quá trình auth bằng fid`)

  }

};
