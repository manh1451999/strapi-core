"use strict";

const fs = require("fs");
const path = require("path");
const mime = require("mime-types");



// config mail template
const templateMemberRegistration = require("../../email template/member_registration");
async function registerEmailTemplate() {  // đọc tempate từ data
  let mailTemplates = await strapi
    .store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions',
      key: 'email',
    })
    .get();

  if (!mailTemplates.member_registration) { // chỉ tạo mới nếu chưa có, đã có hoặc được chỉnh sửa trên ui r thì bỏ qua
    mailTemplates = {
      ...mailTemplates,
      ...templateMemberRegistration
    }

    await strapi
      .store({
        environment: '',
        type: 'plugin',
        name: 'users-permissions',
        key: 'email',
      })
      .set({ value: mailTemplates });

    return;
  }
}


// check run first
async function isFirstRun() {
  const pluginStore = strapi.store({
    environment: strapi.config.environment,
    type: "type",
    name: "setup",
  });
  const initHasRun = await pluginStore.get({ key: "initHasRun" });
  await pluginStore.set({ key: "initHasRun", value: true });
  return !initHasRun;
}


// config role
async function setPublicPermissions(newPermissions) {
  // Find the ID of the public role
  const publicRole = await strapi
    .query("role", "users-permissions")
    .findOne({ type: "public" });

  // List all available permissions
  const publicPermissions = await strapi
    .query("permission", "users-permissions")
    .find({
      type: ["users-permissions", "application"],
      role: publicRole.id,
    });

  // Update permission to match new config
  const controllersToUpdate = Object.keys(newPermissions);
  const updatePromises = publicPermissions
    .filter((permission) => {
      // Only update permissions included in newConfig
      if (!controllersToUpdate.includes(permission.controller)) {
        return false;
      }
      if (!newPermissions[permission.controller].includes(permission.action)) {
        return false;
      }
      return true;
    })
    .map((permission) => {
      // Enable the selected permissions
      return strapi
        .query("permission", "users-permissions")
        .update({ id: permission.id }, { enabled: true });
    });
  await Promise.all(updatePromises);
}





// config view content manager (bảng danh sách, màn hình chi tiết)
async function configView(key, value) {
  const pluginStore = await strapi.store({
    environment: '',
    type: 'plugin',
    name: 'content_manager',
  });
  let data;
  data = await pluginStore.get({ key });
  data = { ...data, ...value };
  return pluginStore.set({ key, value: data })
}



// util tạo file, bản ghi
function getFileSizeInBytes(filePath) {  // tính dung lượng file theo byte
  const stats = fs.statSync(filePath);
  const fileSizeInBytes = stats["size"];
  return fileSizeInBytes;
}

function getFileData(fileName) {   // lấy thông tin chi tiết file
  try {
    if (!fileName) return null;
    const filePath = `./data/uploads/${fileName}`;

    // Parse the file metadata
    const size = getFileSizeInBytes(filePath);
    const ext = fileName.split(".").pop();
    const mimeType = mime.lookup(ext);

    return {
      path: filePath,
      name: fileName,
      size,
      type: mimeType,
    };
  } catch (err) {
    console.log('err', err)
    return null
  }

}

async function createEntry({ model, entry, files }) { // hàm tạo bản ghi
  try {
    const createdEntry = await strapi.query(model).create(entry);
    if (files) {
      await strapi.entityService.uploadFiles(createdEntry, files, {
        model,
      });
    }
  } catch (err) {
    // console.log('err', err)
  }
}

// delete table
async function deteleDataTable({ tableName }) {
  // return strapi.db.query(`api::${tableName}.${tableName}`).deleteMany();
  if (tableName === 'user') return strapi.query('user', 'users-permissions').delete();
  return strapi.query(`${tableName}`).delete();
}

// tạo bản ghi có relation
async function getEntryWithRelations(options) {
  const { entry, listRelations = [] } = options
  let newEntry = { ...entry }
  for (let item of listRelations) {
    const { nameFieldRelation, nameFieldKey = "title", modelRelation, listDataSeed } = item
    let entryRelationSeed = listDataSeed.find(item => item?.id == (entry?.[nameFieldRelation]?.id || entry?.[nameFieldRelation]))
    if ((!newEntry[nameFieldRelation]) || (!entryRelationSeed)) continue;
    let listDataDb = await strapi.query(modelRelation).find();
    let entryRelationDb = listDataDb.find(item => item[nameFieldKey] === entryRelationSeed[nameFieldKey])
    delete newEntry[nameFieldRelation]
    if (entryRelationDb) newEntry[nameFieldRelation] = entryRelationDb.id
  }
  return newEntry;
}



async function importCategories() { // import bản ghi thường
  // await deteleDataTable({ tableName: "category" })
  // return Promise.all(
  //   categorie.map((category) => {
  //     return createEntry({ model: "category", entry: category });
  //   })
  // );
}

async function importPost() {  // import bản ghi có relation

  // await deteleDataTable({ tableName: "post" })
  // const typeNews = await strapi.query("category").find();

  return Promise.all(
    newsPost.map(async (post) => {
      // Get relations for each article
      const entry = await getEntryWithRelations({ entry: post, listRelations: [{ nameFieldRelation: 'categories', nameFieldKey: "name", modelRelation: 'category-news', listDataSeed: categoriesNews }] })
      const files = {
        thumbnail: getFileData(`${post.slug}.jpg`),
      };

      return createEntry({
        model: "news-post",
        entry,
        files,
      });
    })
  );
}




function configPublicPermisson() {
  strapi.log.info('Setting up the public permisson...');
  // return setPublicPermissions({
  //   "category-document": ["find", "findone", "count"],
  // });
}

async function configViews() {
  strapi.log.info('Setting the admin layout...')
  // await configView("configuration_content_types::application::review.review", require("../../views/review.json"))

}


async function importSeedData() {
  strapi.log.info('seeding data ...')
  // await importCategories()
}

module.exports = async () => {


  try {
    // const shouldImportSeedData = true
    await configPublicPermisson()
    await registerEmailTemplate()
    const shouldImportSeedData = await isFirstRun();

    if (shouldImportSeedData) {
      await configViews()
      try {
        await importSeedData();
        console.log("Ready to go");
      } catch (error) {
        console.log("Config fail");
        console.error(error);
      }
    }
  } catch (err) {
    console.log('err', err)
  }

};
