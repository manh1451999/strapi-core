module.exports = {
  //...
  settings: {
    cors: {
      enabled: true,
      headers: '*',
      // origin: ["http://localhost", 'https://foo.example'],
    },
    parser: {
      enabled: true,
      multipart: true,
      formidable: {
        maxFileSize: 1 * 1024 * 1024 * 1024 // Defaults to 200mb
      }
    },
    custom: {
      enabled: true,
      headers : [ 'Content-Type', 'Authorization', "fidtoken", "fidToken", "tokenfid", "tokenFid", 'Origin', 'Accept', 'Cache-Control' ] // cho phép đính kèm các field khác vào header
    }
  },
  //...
  load: {
    after: ['custom']
  }
}
