const axios = require('axios');
const { authFid } = require('../../utils/fid');
const handleErrors = (ctx, err = undefined, type) => {
  throw strapi.errors[type](err);
};


module.exports =  async (ctx, next) => {
  try {
    await authFid(ctx)
    if(ctx.state.user ) return await next();
    return handleErrors(ctx, undefined, 'unauthorized');

  }
  catch (err) {
    return handleErrors(ctx, err, 'unauthorized');
  }
}
