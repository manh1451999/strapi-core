module.exports =  async (ctx, next) => {

  try{
    if (ctx.is('multipart')) {
      if (ctx?.request?.body?.data) {
        let dataParse = JSON.parse(ctx?.request?.body?.data) || {}
        dataParse.fid = ctx?.state?.user?.fid
        ctx.request.body.data = JSON.stringify(dataParse)
      }
    }
    else{
      if(ctx.request.body) ctx.request.body.fid = ctx.state.user?.fid
    }
  }
  catch(err){
    console.log('err', err)
  }

  return await next();
}
