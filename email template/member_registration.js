const templateMemberRegistration = {
  "member_registration": {
    "display": "Email.template.member_registration",
    "icon": "check-square",
    "options": {
      "from": {
        "name": "CSKH",
        "email": "cskh@ftech.ai"
      },
      "response_email": "cskh@ftech.ai",
      "object": "Đây là tiêu đề",
      "message": `
    <p>Kính gửi <%= name %></p>\n\n
    <p>
    Đây là nội dung
    </p>\n\n
    <p>Xin cảm ơn.</p>
        `
    }
  }
}

module.exports = templateMemberRegistration
