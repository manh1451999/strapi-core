const templateRecruitRegistration = {
  "recruit_registration": {
    "display": "Email.template.recruit_registration",
    "icon": "check-square",
    "options": {
      "from": {
        "name": "Fschool",
        "email": "cskh@ftech.ai"
      },
      "response_email": "cskh@ftech.ai",
      "object": "Người dùng đăng ký tuyển dụng",
      "message": `
    <p>Kính gửi <%= name %></p>\n\n
    <p>
    Người dùng đăng ký tuyển dụng
    </p>\n\n
    <p>Xin cảm ơn.</p>
        `
    }
  }
}

module.exports = templateRecruitRegistration
