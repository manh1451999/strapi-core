import colorConfig, { colors } from './color';
module.exports = {
  ////// CONFIGURATION TEMPLATE: uncomment desired lines to override default config.
  ////// While all lines are commented, config will not change anything.
  toolbar: {
    // ['alignment', 'autoformat', 'autoLink', 'blockQuote', 'bold', 'code', 'codeBlock', 'essentials', 'findAndReplace', 'fontBackgroundColor', 'fontColor', 'fontFamily', 'fontSize', 'heading', 'highlight', 'horizontalLine', 'htmlEmbed', 'image', 'imageCaption', 'imageInsert', 'imageResize', 'imageStyle', 'imageToolbar', 'imageUpload', 'indent', 'indentBlock', 'italic', 'link', 'linkImage', 'list', 'listProperties', 'mathType', 'mediaEmbed', 'mediaEmbedToolbar', 'paragraph', 'pasteFromOffice', 'removeFormat', 'sourceEditing', 'specialCharacters', NaN, 'strikethrough', 'subscript', 'superscript', 'table', 'tableCaption', 'tableCellProperties', 'tableColumnResize', 'tableProperties', 'tableToolbar', 'textTransformation', 'todoList', 'underline', 'strapiUploadAdapter', 'strapiMediaLib', 'fullScreen']
    items: [
        "heading",
        "|",
        "fontFamily",
        "fontSize",
        "fontColor",
        'fontBackgroundColor',
        "|",
        "bold",
        "italic",
        "underline",
        "sourceEditing",
        "link",
        "bulletedList",
        "numberedList",
        "insertImage",
        "strapiMediaLib",
        "|",
        "alignment",
        "indent",
        "outdent",
        "|",
        "insertTable",
        "mediaEmbed",
        "htmlEmbed",
        "blockQuote",
        "horizontalLine",
        "|",
        'MathType',
			  'ChemType',
        "fullScreen",
        "undo",
        "redo",
        "specialCharacters",
        "codeBlock",
        "findAndReplace",
        "highlight",
        "removeFormat",
        "strikethrough",
        "subscript",
        "superscript",
        "todoList"

    ],
    shouldNotGroupWhenFull: true
  },
  fontColor: colorConfig,
  fontBackgroundColor: colorConfig,
  	fontSize: {
		options: [
			8,
			9,
			10,
			11,
			12,
			14,
			16,
			18,
			20,
			24,
			26,
			28,
			36,
			48,
			'default',
		]
	},
  image: {
    // styles: [
    //     "alignLeft",
    //     "alignCenter",
    //     "alignRight",
    // ],
    // resizeOptions: [
    //     {
    //         name: "resizeImage:original",
    //         value: null,
    //         icon: "original"
    //     },
    //     {
    //         name: "resizeImage:50",
    //         value: "50",
    //         icon: "medium"
    //     },
    //     {
    //         name: "resizeImage:75",
    //         value: "75",
    //         icon: "large"
    //     }
    // ],
    // toolbar: [
    //     "imageStyle:alignLeft",
    //     "imageStyle:alignCenter",
    //     "imageStyle:alignRight",
    //     "|",
    //     "imageTextAlternative",
    //     "|",
    //     "resizeImage:50",
    //     "resizeImage:75",
    //     "resizeImage:original",
    //     "|",
    //     "linkImage",
    // ]
  },
  table: {
    // contentToolbar: [
    //     "tableColumn",
    //     "tableRow",
    //     "mergeTableCells",
    //     "tableProperties",
    //     "tableCellProperties",
    // ]
  },
  heading: {
    options: [
        { model: "paragraph", title: "Paragraph", class: "ck-heading_paragraph" },
        { model: "heading1", view: "h1", title: "Heading 1 (h1)", class: "ck-heading_heading1" },
        { model: "heading2", view: "h2", title: "Heading 2 (h2)", class: "ck-heading_heading2" },
        { model: "heading3", view: "h3", title: "Heading 3 (h3)", class: "ck-heading_heading3" },
        { model: "heading4", view: "h4", title: "Heading 4 (h4)", class: "ck-heading_heading4" },
    ]
  },
  htmlEmbed: {
    // showPreviews: true,
  },
  mediaEmbed: {
    previewsInData: true
    },
  fontFamily: {
    // options: [
    //   "default",
    //   "Arial, Helvetica, sans-serif",
    //   "Courier New, Courier, monospace",
    //   "Georgia, serif",
    //   "Lucida Sans Unicode, Lucida Grande, sans-serif",
    //   "Tahoma, Geneva, sans-serif",
    //   "Times New Roman, Times, serif",
    //   "Trebuchet MS, Helvetica, sans-serif",
    //   "Verdana, Geneva, sans-serif",
    //   "JetBrains Mono, monospace",
    //   "Lato, Inter, sans-serif",
    // ],
  },
  link: {
    defaultProtocol: "http://",
    decorators: [
      {
        mode: "manual",
        label: "Open in a new tab",
        defaultValue: true,
        attributes: {
          target: "_blank",
          rel: "noopener noreferrer nofollow ",
        },
      },
      {
        mode: "manual",
        label: "Downloadable",
        attributes: {
          download: "download",
        },
      },
    ],
  },
};
