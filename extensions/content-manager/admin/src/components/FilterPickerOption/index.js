import React, { memo, useMemo, useEffect, useCallback } from 'react';
import { get, isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { CircleButton, getFilterType } from 'strapi-helper-plugin';
import { Select } from '@buffetjs/core';

import { InputWrapper, Wrapper } from './components';
import Input from './Input';
import Option from './Option';
import { COLUMNS_FILTER, findEnumCustom } from '../../utils/constants';

const styles = {
  select: {
    minWidth: '170px',
    maxWidth: '200px',
  },
  selectMiddle: {
    minWidth: '130px',
    maxWidth: '200px',
    marginLeft: '10px',
    marginRight: '10px',
  },
};

function FilterPickerOption({
  contentType, // truyền từ cha xuống
  allowedAttributes: allowedAttributesOrg,
  modifiedData,
  index,
  onChange,
  onClickAddFilter,
  onRemoveFilter,
  value,
  showAddButton,
  type,
}) {
  const currentFilterName = get(modifiedData, [index, 'name'], '');


  const ENUM_CUSTOM = useMemo(() => {
    return findEnumCustom({model: contentType.apiID, fieldName: currentFilterName})
  }, [currentFilterName, contentType.apiID])

  // component này là 3 ô select nằm ngang lúc filter
  const IGNORE_FILTER = useMemo(() => {
    if (ENUM_CUSTOM) return ['_ne', '_lt', '_lte', '_gt', '_gte', '_contains']       // tuỳ chỉnh bỏ lọc với các trường chỉ định
    if (type === 'datetime' || type === 'date' || type === 'timestamp' || type === 'integer') return ['_ne', '_contains']
    return ['_ne', '_lt', '_lte', '_gt', '_gte', '_contains']
  }, [type, currentFilterName, contentType.apiID])

  const filtersOptions = getFilterType(type).filter(i => !IGNORE_FILTER.includes(i.value)).reverse()
  const allowedAttributes = useMemo(() => {
    let columnsFilterCustom = COLUMNS_FILTER.find(item => item.model === contentType.apiID)?.columns
    if (columnsFilterCustom) return allowedAttributesOrg.filter(item => columnsFilterCustom.includes(item.name));   // nếu có trong file constant thì chỉ lấy những trường đó, không có trong file constant mặc định lấy hết
    return allowedAttributesOrg
  }, [allowedAttributesOrg])
  const currentFilterData = allowedAttributes.find(attr => attr.name === currentFilterName);
  const options = get(currentFilterData, ['options'], null) || ['true', 'false'];


  const optionsCustom = useMemo(() => {
    if (!!ENUM_CUSTOM) {
      return ENUM_CUSTOM.options
    }
    return options
  }, [currentFilterName])

  useEffect(() => {
    if (!!ENUM_CUSTOM) {
      onChange({
        target: {
          name: `${index}.value`,
          value: optionsCustom[0].value
        }
      })
    }

  }, [currentFilterName])

  return (
    <Wrapper borderLeft={!isEmpty(value)}>
      <InputWrapper>
        <CircleButton type="button" isRemoveButton onClick={() => onRemoveFilter(index)} />
        <Select
          onChange={e => {
            // Change the attribute
            onChange(e);
            // Change the default filter so it reset to the common one which is '='
            onChange({ target: { name: `${index}.filter`, value: '=' } });
          }}
          name={`${index}.name`}
          value={currentFilterName}
          options={allowedAttributes.map(attr => attr.name)}
          style={styles.select}
        />
        <Select
          onChange={onChange}
          name={`${index}.filter`}
          options={filtersOptions.map(option => (
            <Option {...option} key={option.value} />
          ))}
          style={styles.selectMiddle}
          value={get(modifiedData, [index, 'filter'], '')}
        />
        { !!ENUM_CUSTOM ? <Select
          onChange={onChange}
          name={`${index}.value`}
          options={optionsCustom}
          style={styles.selectMiddle}
          value={get(modifiedData, [index, 'value'], '')}
        /> : <Input
          type={type}
          name={`${index}.value`}
          value={get(modifiedData, [index, 'value'], '')}
          options={optionsCustom}
          onChange={onChange}
        />}
        {showAddButton && <CircleButton type="button" onClick={onClickAddFilter} />}
      </InputWrapper>
    </Wrapper>
  );
}

FilterPickerOption.defaultProps = {
  allowedAttributes: [],
  modifiedData: [],
  index: -1,
  onChange: () => { },
  onClickAddFilter: () => { },
  onRemoveFilter: () => { },
  value: null,
  type: 'string',
};

FilterPickerOption.propTypes = {
  contentType: PropTypes.object.isRequired,
  allowedAttributes: PropTypes.array,
  modifiedData: PropTypes.array,
  index: PropTypes.number,
  onChange: PropTypes.func,
  onClickAddFilter: PropTypes.func,
  onRemoveFilter: PropTypes.func,
  showAddButton: PropTypes.bool.isRequired,
  type: PropTypes.string,
  value: PropTypes.any,
};

export default memo(FilterPickerOption);
