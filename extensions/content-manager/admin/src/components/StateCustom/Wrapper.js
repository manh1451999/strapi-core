import styled from 'styled-components';
import {Text} from '@buffetjs/core';
import { COLOR_STATUS } from '../../utils/constants';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  min-width: 90px;
  padding: 1rem;
  border-radius: 0.2rem;
  height: 2.5rem;
  ${({theme, color}) => {

    return `
      border: 1px solid ${COLOR_STATUS[color].borderColor};
      background-color: ${COLOR_STATUS[color].backgroundColor};
      ${Text} {
          color: ${COLOR_STATUS[color].color};
      }
    `
    }
  };
`;

export default Wrapper;
