import React from 'react';
import PropTypes from 'prop-types';
import { Text } from '@buffetjs/core';
import Wrapper from './Wrapper';

const StateCustom = ({ text, color }) => {
  return (
    <Wrapper color={color}>
      <Text lineHeight="19px">
        {text}
      </Text>
    </Wrapper>
  );
};

StateCustom.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,
  color: PropTypes.string,
};

export default StateCustom;
