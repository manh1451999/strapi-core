
const CONSTANTS = {

  COLOR_STATUS: {
    green: {
      borderColor: '#aad67c',
      backgroundColor: '#e6f8d4',
      color: '#6dbb1a',
    },
    blue: {
      borderColor: "#0097f7",
      backgroundColor: "#e6f0fb",
      color: "#0097f7",
    },
    red: {
      borderColor: '#FF90A3',
      backgroundColor: '#FFC4CE',
      color: '#ff4263',
    },
    orange: {
      borderColor: '#f6d75f',
      backgroundColor: '#fff2d1',
      color: 'orange',
    },
  },
  ENUM_FIELD_CUSTOM: [
    {
      field: 'enumCustom',
      model: ['test'],
      options: [
        {
          label: "1. Đây là label custom",
          value: 1,
        },
        {
          label: "2. Đây là label custom 4",
          value: 2,
        },
        {
          label: "3. Đây là label custom 3",
          value: 3,
        },
      ]
    }
  ],
  findEnumCustom(options) {
    const { model, fieldName } = options
    const ENUM_CUSTOM = CONSTANTS.ENUM_FIELD_CUSTOM.find(item => (item.field == fieldName && (item.model || [])?.includes(model)))
    return ENUM_CUSTOM
  },

  COLUMNS_FILTER: [
    {
      model: "test",
      columns: ['id', 'title', 'created_at', 'status', 'enumCustom']      // giới hạn lọc
    },

  ],
  CUSTOM_VALIDATE_REQUIRED_DEFINED: {
    "application::test.test": [
      "users"
    ]
  }
}
module.exports = CONSTANTS

// STATUS_CUSTOM

