import React, { useMemo } from 'react'
import styled from 'styled-components';
import { LeftMenuHeader } from '../../components/LeftMenu';
import MenuLinkSection from '../../components/LeftMenu/LeftMenuCustom/MenuLinkSection';
import { ROLE } from '../../utils/constants';
import getRoleUser from '../../utils/getRoleUser';
import './customMenu.css'
export default function LeftMenuCustomContainer({filteredCollectionTypeLinks, filteredSingleTypeLinks}) {
  const menuDefault = [
    {
      title: "Quản lý trang",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/singleType/application::home-page.home-page",
          "isDisplayed": true,
          "label": "Trang homepage",
          "search": ""
        },

        {
          "icon": "book",
          "destination": "/plugins/content-manager/singleType/application::club-page.club-page",
          "isDisplayed": true,
          "label": "Trang Club",
          "search": ""
        },

      ]
    },

    {
      title: "Đăng ký",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::subject-event.subject-event",
          "isDisplayed": true,
          "label": "Đối tượng",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::category-event.category-event",
          "isDisplayed": true,
          "label": "Loại sự kiện",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::role-event.role-event",
          "isDisplayed": true,
          "label": "Chức vụ liên minh",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/singleType/application::package-support-league.package-support-league",
          "isDisplayed": true,
          "label": "Banner gói hỗ trợ liên minh",
          "search": ""
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::event.event",
          "isDisplayed": true,
          "label": "Đăng ký sự kiện",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::leader-provincial.leader-provincial",
          "isDisplayed": true,
          "label": "Đăng ký leader tỉnh",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::campus.campus",
          "isDisplayed": true,
          "label": "Đăng ký campus",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::league.league",
          "isDisplayed": true,
          "label": "Đăng ký liên minh",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
      ]
    },
    {
      title: "Sự kiện đang diễn ra",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::event.event",
          "isDisplayed": true,
          "label": "Danh sách sự kiện",
          "search": "page=1&pageSize=10&_sort=id:DESC&status=3"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::team-event.team-event",
          "isDisplayed": true,
          "label": "Danh sách đội tham gia",  // gồm cả danh sách các đội đăng ký tham gia
          "search": "page=1&pageSize=10&_sort=id:DESC"
        }


      ]
    },
    {
      title: "Hội quán",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::club.club",
          "isDisplayed": true,
          "label": "Danh sách hội quán",     // gồm cả danh sách đăng ký trở thành hội quán
          "search": "page=1&pageSize=10&_sort=id:DESC"
        }
      ]
    },
    {
      title: "Tin tức thư viện",
      collectionLinks: [

        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::article.article",
          "isDisplayed": true,
          "label": "Tin tức",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        }
      ]
    },
    {
      title: "Chính sách hỗ trợ",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::policy.policy",
          "isDisplayed": true,
          "label": "Đăng bài",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::subject-policy.subject-policy",
          "isDisplayed": true,
          "label": "Đối tượng",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::category-policy.category-policy",
          "isDisplayed": true,
          "label": "Category",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },

      ]
    },
    {
      title: "Bảng xếp hạng",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::rank.rank",
          "isDisplayed": true,
          "label": "Xếp hạng",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::team-rankings.team-rankings",
          "isDisplayed": true,
          "label": "Danh sách đội",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
      ]
    },
    {
      title: "Báo cáo",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::report.report",
          "isDisplayed": true,
          "label": "Hòm thư góp ý",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        }
      ]
    },
    {
      title: "Quản lý địa chỉ",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::city.city",
          "isDisplayed": false,
          "label": "Thành phổ",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::district.district",
          "isDisplayed": false,
          "label": "Danh sách quận huyện",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
      ]
    },

    {
      title: "Quản lý trạng thái",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::status.status",
          "isDisplayed": false,
          "label": "Trạng thái",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },

      ]
    },
    {
      title: "Người dùng",
      collectionLinks: [

        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/plugins::users-permissions.user",
          "isDisplayed": false,
          "label": "Người dùng ứng dụng",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
        {
          "icon": "book",
          "destination": "/settings/users",
          "isDisplayed": false,
          "label": "Người dùng hệ thống",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },
      ]
    },
    {
      title: "Các trang ẩn",
      collectionLinks: [
        {
          "icon": "book",
          "destination": "/plugins/content-manager/collectionType/application::status.status",
          "isDisplayed": false,
          "label": "Trạng thái",
          "search": "page=1&pageSize=10&_sort=id:DESC"
        },

      ]
    },

  ]

  const otherMenu = useMemo(()=>{
    let results = []
    let listItemMenuCustom = [].concat(...menuDefault.map(item=> item.collectionLinks))
    let listItemMenuCollectionOther = filteredCollectionTypeLinks.filter(menu=> !listItemMenuCustom.find(x=>menu.destination == x.destination))
    let listItemMenuSingleOther = filteredSingleTypeLinks.filter(menu=> !listItemMenuCustom.find(x=>menu.destination == x.destination))
    if(listItemMenuCollectionOther.length > 0) results.push({title: "Các thành phần khác", collectionLinks: listItemMenuCollectionOther})
    if(listItemMenuSingleOther.length > 0) results.push({title: "Các trang khác", collectionLinks: listItemMenuSingleOther})
    return results
  }, [filteredCollectionTypeLinks, filteredSingleTypeLinks])

  const menuEditor = [{
    title: "Plugins",
    collectionLinks: [
      {
        destination: "/plugins/upload",
        icon: "cloud-upload-alt",
        isDisplayed: true,
        label: "Media Libarary",
      }
    ]
  }
 ]
  const menuAdmin = [{
    title: "Plugins",
    collectionLinks: [
      {
        destination: "/plugins/content-type-builder",
        icon: "paint-brush",
        isDisplayed: true,
        label: "Content-Types Builder",
      },
      {
        destination: "/plugins/documentation",
        icon: "book",
        isDisplayed: true,
        label: "Documentation",
      },
      {
        destination: "/plugins/upload",
        icon: "cloud-upload-alt",
        isDisplayed: true,
        label: "Media Libarary",
      }
    ]
  },
  {
    title: "General",
    collectionLinks: [
      {
        destination: "/list-plugins",
        icon: "list",
        isDisplayed: true,
        label: "Plugins",
      },
      {
        destination: "/marketplace",
        icon: "shopping-basket",
        isDisplayed: true,
        label: "Marketplace",
      },
      {
        destination: "/settings",
        icon: "cog",
        isDisplayed: true,
        label: "Settings",
      }
    ]
  }]

  // const menu = useMemo(() => {
  //   let role = getRoleUser();
  //   if (role === ROLE.SUPER_ADMIN) return [...menuDefault, ...menuAdmin]
  //   else if (role === ROLE.EDITOR) return [...menuDefault, ...menuEditor]
  //   return menuDefault
  // }, [])

  const menu = useMemo(() => {
    let role = getRoleUser();
    if (role === ROLE.SUPER_ADMIN) return [...menuDefault, ...otherMenu, ...menuAdmin]
    else if (role === ROLE.EDITOR) return [...menuDefault, ...otherMenu, ...menuEditor]
    return menuDefault
  }, [otherMenu])

  const menuFilter = useMemo(() => {
    return menu.map(item=>{
      let newItem = {...item}
      newItem.collectionLinks = newItem.collectionLinks.filter(collectionLink=>collectionLink?.isDisplayed)
      return newItem
    }).filter(item=> item?.collectionLinks?.length>0)
  }, [menu])



  return (
    <div className="left-menu-cunstom">
      <Wrapper>
        <LeftMenuHeader />
        <LinksContainer>
          {
            menuFilter.map((itemMenu, index) => {
              return <MenuLinkSection key={`menuLinkSection.${index}`} title={itemMenu?.title} collectionLinks={itemMenu?.collectionLinks} />
            })
          }


        </LinksContainer>
      </Wrapper>
    </div>

  );
};



const Wrapper = styled.div`
  position: fixed;
  float: left;
  top: 0;
  left: 0;
  height: 100vh;
  width: ${props => props.theme.main.sizes.leftMenu.width};
  background: #fff;

  /* scrollbar overrides */
  * {
    ::-webkit-scrollbar {
      width: 7px;
    }

    ::-webkit-scrollbar-track,
    ::-webkit-scrollbar-track:hover {
      background-color: transparent;
    }

    ::-webkit-scrollbar-thumb {
      background-color: ${props => props.theme.main.colors.leftMenu['title-color']};
    }

    ::-webkit-scrollbar-thumb:hover {
      background-color: ${props => props.theme.main.colors.leftMenu['link-color']};
    }

    /* firefox */
    scrollbar-color: ${props => props.theme.main.colors.leftMenu['title-color']} transparent;
  }
`;

const LinksContainer = styled.div`
  padding-top: 3rem;
  position: absolute;
  top: ${props => props.theme.main.sizes.leftMenu.height};
  right: 0;
  bottom: 0;
  left: 0;
  overflow-y: auto;
  height: calc(100vh - (${props => props.theme.main.sizes.leftMenu.height} + 3rem));
  box-sizing: border-box;
`;
