export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const ROLE = {
  SUPER_ADMIN: 'strapi-super-admin',
  EDITOR: 'strapi-editor',
  AUTHOR: 'strapi-author',
  GUEST: 'guest'
}
