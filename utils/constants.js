module.exports = {

  COLOR_STATUS:{
    green: {
      borderColor: '#aad67c',
      backgroundColor: '#e6f8d4',
      color: '#6dbb1a',
    },
    blue: {
      borderColor: "#0097f7",
      backgroundColor: "#e6f0fb",
      color: "#0097f7",
    },
    red: {
      borderColor: '#FF90A3',
      backgroundColor: '#FFC4CE',
      color: '#ff4263',
    },
    orange: {
      borderColor: '#f6d75f',
      backgroundColor: '#fff2d1',
      color: 'orange',
    },
  },

  STATUS_CUSTOM: [
    {
      label: "Nháp",
      slug: 'draft',
      value: 1,
      color: 'orange'
    },
    {
      label: "Hoàn thành",
      slug: 'finished',
      value: 2,
      color: 'blue',
    },
    {
      label: "Đã duyệt",
      slug: 'approved',
      value: 3,
      color: 'green',
    },
    {
      label: "Thất bại",
      slug: 'reject',
      value: 4,
      color: 'red',
    },


  ],
  COLUMNS_FILTER: [
    {
      model: "test",
      columns: ['id', 'title', 'created_at', 'status']      // giới hạn lọc
    },

  ],
  LIST_MODEL_HAS_STATUS: ["test"]
}
