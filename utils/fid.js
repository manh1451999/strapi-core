const axios = require('axios');
var Axios = require('axios');

const handleErrors = (ctx, err = undefined, type) => {
  throw strapi.errors[type](err);
};

const authFid = async (ctx) => {
  try {
    let token = ctx?.request?.header?.tokenfid

    if (!token) throw new Error('Invalid token: Token did not contain required fields, please add field tokenfid or fidtoken to header');

    if (ctx.request && ctx.request.header && ctx.request.header.tokenfid) {
      const response = await axios.get(`${process.env.FID_URL}/connect/userinfo`, { headers: { Authorization: `Bearer ${token}` } })
      if (response?.data) {
        let user = ctx.state?.user || {}
        ctx.state.user = { ...user, ...response?.data, fid: response?.data?.sub }
      }
    }

  }
  catch (err) {
    console.log('err', err)
    return handleErrors(ctx, err, 'unauthorized');
  }
}


/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */
async function getCachedData(key) {
    try {
        const now = new Date();
        const pluginStore = strapi.store({
            environment: strapi.config.environment,
            type: "api",
            name: "club",
        });

        const itemStr = await pluginStore.get({ key: key })
        if (!itemStr) {
            return null;
        }
        let duration = (now.getTime() - itemStr.start) / 1000;
        if (duration > itemStr.expries_in) {
            pluginStore.delete({ key: key })
            return null;
        }
        return itemStr.valueToken;

    } catch (ex) {
        console.log(ex)
    }
}
async function setWithExpiry(key, value, ttl) {
    const now = new Date();
    const pluginStore = strapi.store({
        environment: strapi.config.environment,
        type: "api",
        name: "club",
    });
    await pluginStore.set({
        key: key,
        value: {
            valueToken: value,
            expries_in: ttl,
            start: now.getTime(),
        }
    });
}
async function getTokenFid() {    // sử dụng để kết nối dạng server to server
    const KEY = `COMMUNITY_ACCESS_TOKEN`
    const getTokenFromLocalstorage = await getCachedData(KEY);
    if (getTokenFromLocalstorage) {
        return getTokenFromLocalstorage;
    }
    const params = new URLSearchParams()
    params.append('client_id', process.env.FID_PARAMS_CLIENT_ID)
    params.append('client_secret', process.env.FID_PARAMS_CLIENT_SECRET)
    params.append('grant_type', process.env.FID_PARAMS_GRANT_TYPE)
    // params.append('scopes', process.env.FID_PARAMS_SCOPES)
    const url = `${process.env.FID_URL}/connect/token`
    let access_token

    const getToken = await Axios.post(url, params, {
        headers: {
            Accept: "*/*",
            "Content-Type": "application/x-www-form-urlencoded",
        },
    });
    if (getToken.status === 200) {
        access_token = getToken.data.access_token;
        setWithExpiry(KEY, access_token, getToken.data.expires_in)
        return access_token;
    } else {
        return false;
    }

}



module.exports = {
  authFid,
  getTokenFid
}
